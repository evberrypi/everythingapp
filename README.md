# everythingapp

This is an application Im writing to play with Flutter and Dart.
It main target at the moment is Android.

Im starting with a simple todos and notes app and will be adding in additional functions

### Features 

[x] Note Taking (create, view, edit, delete)
[x] Todos
[x] Share notes
[x] Search Notes/Todos
[ ] Import/Export Database (OR DB Sync with S3/Nextcloud)
[ ] Add Filepicker to attach photos/files to notes
[ ] Add reminders to todos
[ ] Screen with AI Assistant conversations, suggested recommended content


### Known Issues
- Notes not currently workingn on Linux

## Installation
Run `flutter run` with an android device plugged into your computer

To build for android, run `flutter build apk --target-platform android-arm64`