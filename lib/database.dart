import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'notes.dart';

class DatabaseHelper {
  static Database? _database;

  // Create a database
  Future<Database?> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    String path = join(await getDatabasesPath(), 'notes_database.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute('''
          CREATE TABLE notes(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            title TEXT,
            content TEXT,
            tags TEXT,
            date TEXT
          )
        ''');
      },
    );
  }

  // Insert a note
  Future<int> insertNote(Note note) async {
    Database? db = await database;
    return await db!.insert('notes', note.toMap());
  }

  // Retrieve notes
  Future<List<Note>> getNotes() async {
    Database? db = await database;
    final List<Map<String, dynamic>> maps = await db!.query('notes');
    return List.generate(maps.length, (i) {
      return Note.fromMap(maps[i]);
    });
  }

  // Update a note
  Future<void> updateNote(Note note) async {
    Database? db = await database;
    await db!.update(
      'notes',
      note.toMap(),
      where: 'id = ?',
      whereArgs: [note.id],
    );
  }

  // Delete a note
  Future<void> deleteNote(int id) async {
    Database? db = await database;
    await db!.delete(
      'notes',
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
