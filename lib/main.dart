import 'package:flutter/material.dart';
import 'todos.dart';
import 'notes.dart';

void main() => runApp(TodoApp());

class TodoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'EVApp',
      theme: ThemeData(
        primarySwatch: Colors
            .blue, // You can choose other colors like Colors.red, Colors.green, etc.
      ),
      home: DefaultTabController(
        length: 2,
        child: TodoHomePage(),
      ),
    );
  }
}

class TodoHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('EvApp'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: TabBarView(
        children: [
          NotesPage(),
          TodoListPage(),
        ],
      ),
      bottomNavigationBar: TabBar(
        labelColor: Theme.of(context).primaryColor,
        unselectedLabelColor: Colors.green,
        tabs: [
          Tab(icon: Icon(Icons.note), text: 'Notes'),
          Tab(icon: Icon(Icons.list), text: 'Todos'),
        ],
      ),
    );
  }
}
