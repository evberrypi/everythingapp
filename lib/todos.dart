import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum TodoStatus {
  inProgress,
  toDo,
  done,
}

class Todo {
  String title;
  TodoStatus status;

  Todo(this.title, this.status);
}

class TodoListPage extends StatefulWidget {
  @override
  _TodoListPageState createState() => _TodoListPageState();
}

class _TodoListPageState extends State<TodoListPage> {
  final List<Todo> _todos = [];
  final TextEditingController _textController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadTodos(); // Load todos when the app starts
  }

  Future<void> _saveTodos() async {
    final prefs = await SharedPreferences.getInstance();
    final todoStrings = _todos.map((todo) => todo.title).toList();
    prefs.setStringList('todos', todoStrings);
  }

  Future<void> _loadTodos() async {
    final prefs = await SharedPreferences.getInstance();
    final todoStrings = prefs.getStringList('todos') ?? [];
    setState(() {
      _todos.clear();
      _todos.addAll(todoStrings.map((title) => Todo(title, TodoStatus.toDo)));
    });
  }

  void _addTodo() {
    setState(() {
      if (_textController.text.isNotEmpty) {
        _todos.add(Todo(_textController.text, TodoStatus.toDo));
        _textController.clear();
        _saveTodos(); // Save todos after adding a new one
      }
    });
  }

  List<Widget> _buildTodosForStatus(TodoStatus status, String header) {
    List<Todo> todosForStatus =
        _todos.where((todo) => todo.status == status).toList();
    if (todosForStatus.isEmpty) return [];
    return [
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        child: Text(header,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
      ),
      ...todosForStatus.map((todo) => _buildTodoItem(todo)).toList(),
    ];
  }

  Widget _buildTodoItem(Todo todo) {
    return ListTile(
      title: Text(todo.title),
      trailing: PopupMenuButton<String>(
        onSelected: (String result) {
          switch (result) {
            case 'Edit':
              // Call your edit function here
              break;
            case 'Delete':
              setState(() {
                _todos.remove(todo);
                _saveTodos();
              });
              break;
            case 'Set Status: In Progress':
              setState(() {
                todo.status = TodoStatus.inProgress;
                _saveTodos();
              });
              break;
            case 'Set Status: To Do':
              setState(() {
                todo.status = TodoStatus.toDo;
                _saveTodos();
              });
              break;
            case 'Set Status: Done':
              setState(() {
                todo.status = TodoStatus.done;
                _saveTodos();
              });
              break;
          }
        },
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
          const PopupMenuItem<String>(
            value: 'Edit',
            child: Text('Edit'),
          ),
          const PopupMenuItem<String>(
            value: 'Delete',
            child: Text('Delete'),
          ),
          if (todo.status != TodoStatus.inProgress)
            const PopupMenuItem<String>(
              value: 'Set Status: In Progress',
              child: Text('Set Status: In Progress'),
            ),
          if (todo.status != TodoStatus.toDo)
            const PopupMenuItem<String>(
              value: 'Set Status: To Do',
              child: Text('Set Status: To Do'),
            ),
          if (todo.status != TodoStatus.done)
            const PopupMenuItem<String>(
              value: 'Set Status: Done',
              child: Text('Set Status: Done'),
            ),
        ],
      ),
    );
  }

  void _editTodo(BuildContext context, Todo todo) {
    // ... _editTodo method remains the same ...
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: TextField(
                  controller: _textController,
                  decoration: InputDecoration(
                    labelText: 'Enter a task',
                  ),
                  onSubmitted: (text) => _addTodo(),
                ),
              ),
              IconButton(
                icon: Icon(Icons.add),
                onPressed: _addTodo,
              )
            ],
          ),
        ),
        Expanded(
          child: ListView(
            children: [
              ..._buildTodosForStatus(TodoStatus.inProgress, "In Progress"),
              ..._buildTodosForStatus(TodoStatus.toDo, "To-Do"),
              ..._buildTodosForStatus(TodoStatus.done, "Done"),
            ],
          ),
        ),
      ],
    );
  }
}

void main() {
  runApp(MaterialApp(
    title: 'Todo List',
    home: TodoListPage(),
  ));
}
