import 'package:flutter/material.dart';
import 'notes.dart';
import 'noteeditscreen.dart';

class NoteViewScreen extends StatelessWidget {
  final Note note;

  NoteViewScreen({required this.note});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(note.title),
        actions: [
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () {
              // Navigate to the NoteEditScreen
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => NoteEditScreen(
                    note: note,
                    onSave: (editedNote) {
                      // Handle saving the note (update the database, etc.)
                      // You can reuse your existing save logic here
                    },
                  ),
                ),
              );
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              note.title,
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 10),
            Wrap(
              children: note.tags.map((tag) => Text(
                "#$tag ",
                style: TextStyle(color: Colors.lightBlue),
              )).toList(),
            ),
            SizedBox(height: 10),
            Expanded(
              child: Text(note.content),
            ),
          ],
        ),
      ),
    );
  }
}
