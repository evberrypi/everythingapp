import 'package:flutter/material.dart';
import 'notes.dart'; // Replace with the actual path to your Note model
import 'noteeditscreen.dart';

class NotesSearch extends SearchDelegate {
  final List<Note> notes;

  NotesSearch(this.notes);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
          showSuggestions(context);
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    List<Note> results = _searchNotes(query);
    return ListView.builder(
      itemCount: results.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(results[index].title),
          subtitle: Text(results[index].content),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => NoteEditScreen(
                  note: notes[index],
                  onSave: (savedNote) {
                    // Example logic:
                    if (!notes.contains(savedNote)) {
                      notes.add(savedNote);
                      // Optionally save to file if required
                    } else {
                      // Optionally update the file if required
                    }
                  },
                ),
              ),
            );
          },
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<Note> suggestions = _searchNotes(query);
    return ListView.builder(
      itemCount: suggestions.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(suggestions[index].title),
          subtitle: Text(suggestions[index].content),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => NoteEditScreen(
                  note: suggestions[index],
                  onSave: (savedNote) {
                    // Handle saving if required
                  },
                ),
              ),
            );
          },
        );
      },
    );
  }

  List<Note> _searchNotes(String query) {
    return notes.where((note) {
      return note.title.toLowerCase().contains(query.toLowerCase()) ||
          note.content.toLowerCase().contains(query.toLowerCase());
    }).toList();
  }
}
