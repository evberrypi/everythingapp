import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:share/share.dart';
import 'searchnotes.dart';
import 'noteeditscreen.dart';
import 'database.dart';
import 'noteviewscreen.dart';

class Note {
  int? id;
  String title;
  String content;
  List<String> tags;
  DateTime date;

  Note({
    this.id,
    required this.title,
    required this.content,
    required this.tags,
  }) : date = DateTime.now();

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'content': content,
      'tags': tags.join(", "),
      'date': date.toIso8601String(),
    };
  }

  // Extract a Note object from a Map object
  factory Note.fromMap(Map<String, dynamic> map) {
    return Note(
      id: map['id'],
      title: map['title'],
      content: map['content'],
      tags: (map['tags'] as String).split(", "),
    );
  }
}


class NotesPage extends StatefulWidget {
  @override
  _NotesPageState createState() => _NotesPageState();
}

class _NotesPageState extends State<NotesPage> {
  List<Note> _notes = [];
  final db = DatabaseHelper();

  @override
  void initState() {
    super.initState();
    _loadNotes().then((loadedNotes) {
      setState(() {
        _notes = loadedNotes;
      });
    });
  }

  Future<List<Note>> _loadNotes() async {
    return await db.getNotes();
  }

  void _showSearch(BuildContext context) {
    showSearch(context: context, delegate: NotesSearch(_notes));
  }

  void _navigateToEditScreen({Note? note}) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => NoteEditScreen(
          note: note,
          onSave: (editedNote) async {
            if (editedNote.id == null) {
              // New note
              await db.insertNote(editedNote);
            } else {
              // Update existing note
              await db.updateNote(editedNote);
            }
          },
        ),
      ),
    ).then((value) {
      setState(() {
        _loadNotes();
      });
    });
  }

  void _shareNote(Note note) {
    final noteContent =
        '# ${note.title}\n\n${note.content}\n\nTags: ${note.tags.join(", ")}';
    Share.share(noteContent); // Ensure this function is called for sharing
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notes"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () => _showSearch(context),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _navigateToEditScreen(),
        child: Icon(Icons.add),
      ),
      body: ListView.builder(
        itemCount: _notes.length,
        itemBuilder: (context, index) {
          return Slidable(
            actionPane: SlidableDrawerActionPane(),
            actionExtentRatio: 0.25,
            secondaryActions: <Widget>[
              IconSlideAction(
                caption: 'Share',
                color: Colors.green,
                icon: Icons.share,
                onTap: () => _shareNote(_notes[index]),
              ),
              IconSlideAction(
                caption: 'Edit',
                color: Colors.orange,
                icon: Icons.edit,
                onTap: () => _navigateToEditScreen(note: _notes[index]),
              ),
              IconSlideAction(
                caption: 'Delete',
                color: Colors.red,
                icon: Icons.delete,
                onTap: () async {
                  await db.deleteNote(_notes[index].id!);
                  setState(() {
                    _notes.removeAt(index);
                  });
                },
              ),
            ],
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      _notes[index].title,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    "#${_notes[index].tags.join(', ')} - ${_notes[index].date.month}/${_notes[index].date.day}/${_notes[index].date.year}@${_notes[index].date.hour}:${_notes[index].date.minute.toString().padLeft(2, '0')}",
                    style: TextStyle(color: Colors.lightBlue),
                  ),
                ],
              ),
              subtitle: Text(
                '${_notes[index].content.substring(0, _notes[index].content.length < 50 ? _notes[index].content.length : 50)}...',
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => NoteViewScreen(note: _notes[index]),
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}
