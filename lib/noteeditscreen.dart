import 'package:flutter/material.dart';
import 'notes.dart';
import 'database.dart'; // Import the database helper
import 'dart:io';
import 'package:file_picker/file_picker.dart';

class NoteEditScreen extends StatefulWidget {
  final Note? note;
  final Function(Note) onSave;

  NoteEditScreen({this.note, required this.onSave});

  @override
  _NoteEditScreenState createState() => _NoteEditScreenState();
}

class _NoteEditScreenState extends State<NoteEditScreen> {
  TextEditingController _titleController = TextEditingController();
  TextEditingController _contentController = TextEditingController();
  TextEditingController _tagsController = TextEditingController();
  final db = DatabaseHelper();

  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController(text: widget.note?.title ?? "");
    _contentController = TextEditingController(text: widget.note?.content ?? "");
    _tagsController = TextEditingController(text: widget.note?.tags.join(', ') ?? "");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.note == null ? 'Add Note' : 'Edit Note'),
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () async {
              String title = _titleController.text;
              String content = _contentController.text;
              List<String> tags = _tagsController.text.split(',').map((tag) => tag.trim()).toList();

              Note noteToSave = Note(
                title: title,
                content: content,
                tags: tags,
              );

              if (widget.note == null) {
                // New note
                await db.insertNote(noteToSave);
              } else {
                // Existing note - update it
                noteToSave.id = widget.note!.id;
                await db.updateNote(noteToSave);
              }

              Navigator.pop(context); // Return to the previous screen
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              controller: _titleController,
              decoration: InputDecoration(
                labelText: 'Title',
              ),
            ),
            SizedBox(height: 10),
            Expanded(
              child: TextField(
                controller: _contentController,
                decoration: InputDecoration(
                  labelText: 'Content',
                ),
                maxLines: null,
                expands: true,
              ),
            ),
            SizedBox(height: 10),
            TextField(
              controller: _tagsController,
              decoration: InputDecoration(
                labelText: 'Tags (comma separated)',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
